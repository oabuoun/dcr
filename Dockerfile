FROM python:3

RUN apt update
RUN apt-get -y install build-essential tree

RUN apt-get install libgsl0-dev --yes
RUN apt-get install build-essential --yes
RUN apt-get install autotools-dev --yes
RUN apt-get install automake --yes
RUN apt-get install libtool --yes

ADD requirements.txt /dcr/
RUN pip install -r dcr/requirements.txt

ADD src/repetition.py /dcr/src/
ADD src/filesystem.py /dcr/src/
ADD src/logger.py /dcr/src/
ADD src/dispatcher.py /dcr/src/

RUN tree dcr
WORKDIR "/dcr"
ENTRYPOINT [ "python", "src/dispatcher.py" ]
