import json, bitarray, itertools, sys, os.path, operator
import pandas as pd
from itertools import groupby
from io import StringIO
import matplotlib.pyplot as plt
from pprint import pprint
from collections import Counter
import dask
import dask.dataframe as dd
import filesystem as fs
from logger import Logger
from datetime import datetime, time

class Repetition:
	fmt = '%Y-%m-%d %H:%M:%S'
	def __init__(self, parameters):
		self.data_file_path = parameters['data_file_path']
		self.output = "{}/{}".format(parameters['output'], "dcr_results")
		self.strategy = parameters['strategy']
		self.min_threshold = int(parameters['min_threshold'])
		self.max_consecutive = int(parameters['max_consecutive'])
		self.consecutive_min_threshold = 0
		self.calc_max_series_length(parameters['max_series_length'])


		self.max_rep = 0
		self.repetition_matrix = {10: {}, 20: {}, 30: {}, 40: {}, 50: {}, 60: {}, 70: {}, 80: {}, 90: {}, 100: {},}
		self.result_file_name = "{}/{}__{}".format(self.output, fs.name_no_extension(self.data_file_path), "repetition-{}-chunks{}")
		self.result_file_name_byte_position_based = "{}/{}__{}".format(self.output, fs.name_no_extension(self.data_file_path), "repetition-{}-position-based{}")

	def calc_max_series_length(self, max_series_length):
		if self.strategy == "max_series_length":
			try:
				if len(max_series_length) > 0 and max_series_length[-1] == "%":
					self.max_series_length = float(max_series_length[:-1])
					self.max_series_length_percent = True
				else:
					self.max_series_length = int(max_series_length)
					self.max_series_length_percent = False
			except Exception as e:
				self.max_series_length = 1
				self.max_series_length_percent = False

	def run(self):
		launch_timestamp = datetime.now()

		# Create output folder
		fs.mkdirs(self.output)

		# Read the data as bits and converts them to bytes
		self.bytearray_data = self.read_binary_bits(self.data_file_path).tobytes()
		Logger.log("+++ {}: {} Data-Chunk".format(self.data_file_path, len(self.bytearray_data)))

		self.data_chunk_series_length = 1
		stopping_point_reached = False

		while not stopping_point_reached:
			Logger.log("+++ Launching new iteration Length={}".format(self.data_chunk_series_length))

			launch_iteration_timestamp = datetime.now()
			self.launch_iteration()
			stop_iteration_timestamp = datetime.now()

			Logger.log("+++ Iteration Length={} finished in {}".format(self.data_chunk_series_length, stop_iteration_timestamp - launch_iteration_timestamp))

			if self.max_rep <= self.min_threshold:
				self.consecutive_min_threshold += 1
			else:
				self.consecutive_min_threshold = 0
			stopping_point_reached = self.update_stopping_point()
			Logger.log("stopping_point_reached at {} = {}".format(self.data_chunk_series_length, stopping_point_reached))
			self.data_chunk_series_length += 1
		self.save_repetition_matrix()
		end_timestamp = datetime.now()
		Logger.log("+++ File processing finished in {}".format(end_timestamp - launch_timestamp))

	def launch_iteration(self):
		result_file_csv = self.result_file_name.format(self.data_chunk_series_length, ".csv")

		# Get header_columns as list for processing and as a text to write it directly to a csv file
		header_columns, header_column_list, header = self.get_header_columns()

		# Get a list of chunks (Each has {self.data_chunk_series_length} bytes)
		data_chunk_series = self.get_data_chunk_series()


		repetition_list = self.get_repetition_list(data_chunk_series)

		# Save data in a csv file/text
		self.save_repetition_list_csv(header, repetition_list, result_file_csv)

		# Process the CSV using Dask Library, this line load the CSV into {Pandas DataFrame} according to the columns passed in the first argument
		ddf = dd.read_csv(result_file_csv, sep =",", dtype={'Count': int})

		self.max_rep = ddf['Count'].max().compute()

		if self.max_rep > 1:
			ddf.nlargest(len(ddf.index), 'Count').persist().to_csv (result_file_csv, compute = True, single_file = True, index=False)

			# Generate a list (and write it to a file) of Unique_chunks
			self.process_unique_data(ddf, header_column_list)

			result_file_name_byte_position_based = self.result_file_name_byte_position_based.format(self.data_chunk_series_length, ".csv")

			self.process_position_based(ddf, header_column_list, result_file_name_byte_position_based)

			# Generate the summary of repetition results and write it CSV file
			self.generate_summary(ddf)

			# Generate plots for all the results
			self.generate_plots(result_file_name_byte_position_based)
		else:
			fs.delete(result_file_csv)

	def update_stopping_point(self):
		reached = False
		if self.strategy == "max_rep":
			reached = self.max_rep <= self.min_threshold
		elif self.strategy == "max_consecutive":
			reached = self.consecutive_min_threshold >= self.max_consecutive
		elif self.strategy == "max_series_length":
			if self.max_series_length_percent:
				reached = self.data_chunk_series_length >= len(self.bytearray_data) * self.max_series_length / 100
			else:
				reached = self.data_chunk_series_length >= self.max_series_length
		return reached or self.data_chunk_series_length >= len(self.bytearray_data)

	def get_header_columns(self):
		column = "Column-{}{}"
		key_bytes = "".join([column.format(i, ",") for i in range(1, self.data_chunk_series_length + 1)])
		byte_columns = [column.format(i, "") for i in range(1, self.data_chunk_series_length + 1)]
		key_bytes_one_cell = "".join([column.format(i, "|") for i in range(1, self.data_chunk_series_length + 1)]) + ","
		text =  key_bytes_one_cell + key_bytes + "Count" + "\n"
		Logger.log("get_columns: {}".format(text))
		columns = text.rstrip().split(",")
		return columns, byte_columns, text

	def get_data_chunk_series(self):
		# Put the data into data_chunks of {self.data_chunk_series_length} bytes
		data_chunk_series = [self.bytearray_data[x:x+self.data_chunk_series_length] for x in range(0, len(self.bytearray_data), self.data_chunk_series_length)]

		# Delete the last chunk if its length is less than the needed one (self.data_chunk_series_length)
		if len(data_chunk_series[-1]) < self.data_chunk_series_length:
			data_chunk_series = data_chunk_series[:-1]
		Logger.log("++ Data-Chunk-Series: {} data_chunk_series of {} Data-Chunk(s) each".format(len(data_chunk_series), self.data_chunk_series_length))
		return data_chunk_series

	def get_repetition_list(self, data_chunk_series):
		repetition_list = dict(Counter(data_chunk_series))
		return repetition_list


	def save_repetition_list_csv(self, header, repetition_list, result_file_csv):
		line_count = 0
		fs.write(result_file_csv, header, False)
		text = ""
		for data_chunk_series, count in repetition_list.items():
			data_chunk_series_chunks = ",".join([str(b) for b in data_chunk_series])
			data_chunk_series_chunks_one = "|".join([str(b) for b in data_chunk_series]) + "|"
			printable = "{},{},{}".format(data_chunk_series_chunks_one, data_chunk_series_chunks, count)
			text += printable + "\n"
			line_count += 1
			if line_count > 100:
				fs.write(result_file_csv, text, True)
				line_count = 0
				text = ""
		else:
			text = text.rstrip()
			fs.write(result_file_csv, text, True)
		Logger.log("Finished - Repetition List has been saved in {}".format(result_file_csv))


	# Find the Unique Values in Each Data-Chunk of the chunks : 1st-Column, 2nd-Column, ..etc
	def process_unique_data(self, ddf, header_column_list):
		unique_data = {}
		max_len = 0

		for column in list(ddf.columns):
			if column in header_column_list:
				unique_data[column] = sorted(list(ddf[column].unique()))
				max = len(unique_data[column])
				max_len = max if max > max_len else max_len

		summary_header = "Column-"
		summary_count = "Unique Data-Chunk"
		index = 1
		for column in unique_data.keys():
			values = unique_data[column]

			summary_header = "{},{}".format(summary_header, index)
			summary_count  = "{},{}".format(summary_count, len(values))
			index += 1
			for value in range(len(values), max_len):
				unique_data[column].append("")
		unique_data_df = pd.DataFrame.from_dict(unique_data)

		result_file_csv = self.result_file_name.format(self.data_chunk_series_length, "-unique-values.csv")
		unique_data_df.to_csv (result_file_csv, index = False, header=True)

		summary = "{}\n{}".format(summary_header, summary_count)
		result_file_csv = self.result_file_name.format(self.data_chunk_series_length, "-unique-values-summary.csv")
		fs.write(result_file_csv, summary)
		return unique_data

	def process_position_based(self, ddf, header_column_list, result_file_name_byte_position_based):
		if self.data_chunk_series_length == 1:
			ddf = ddf.rename(columns={"Column-1": "Data-Chunk"}).persist()
			ddf = ddf.nsmallest(len(ddf.index), 'Data-Chunk').persist()
			#df = df.sort_values(by=['ASCII'], ascending=True)
			ddf.drop(ddf.columns[0], axis=1).to_csv(result_file_name_byte_position_based, compute = True, single_file = True, index=False)
		else:
			data = {}
			for column in header_column_list:
				values = list(ddf[column])
				#Logger.log("++ Values: {}".format(values))
				data[column] = self.fill_missing_bytes(dict(Counter(values)))

			data_df = pd.DataFrame.from_dict(data)
			data_df.index.name = "Data-Chunk"
			data_df = data_df.sort_values(by=['Data-Chunk'], ascending=True)
			data_df.to_csv (result_file_name_byte_position_based, index = True, header=True)

	def generate_summary(self, df):
		# Convert Pandas DataFrame to Dask DataFrame
		#df = dd.from_pandas(df, npartitions= self.data_chunk_series_length)

		# Calculate the sum of all repetitions (it should be equal to number of chunks)
		total_data_values = df['Count'].sum().compute()

		# Add new column that contains the percent of repetition for each chunk
		percent = df['Count']/total_data_values*100
		df['Percent'] = percent

		# Calculate rows count (it should be equal to number of unique values)
		total_row_count = len(df.index)

		# Sort the Data - Descending order of the percent
		df = df.nlargest(total_row_count, 'Percent').compute()

		# This is the header of the CSV file
		text = "Unique Series,Unique Series % Total Data,% Unique Series,Repetition,Repetition%,Total Data,Percentage\n"

		# What percentages should be calculated
		percentages = [10, 20 , 30, 40, 50, 60, 70, 80, 90]

		# Current Percent - initial 0
		percent_sum = 0

		# How many row should be taken into sum
		row_count = 1

		# A list to save the results (how many rows, count and real percentages) of each round
		percentage_row_counts = []
		Logger.log("percentages={}".format(percentages))

		# Go through percentages one by one
		# Keep adding rows and calculate the sum of percents until it's the nearest to the needed percentage
		for percentage in percentages:

			Logger.log("percentage={}, total_row_count={}, percent_sum={}, row_count={}, percentage_row_counts={}".format(percentage, total_row_count, percent_sum, row_count, percentage_row_counts))

			# Difference between the needed percentage and the current percentage
			difference = percentage

			# Save a list of all percentages found, this helps to prevent getting stuck in an infinite loop
			percentage_records = []

			# True when it should stops because we have the nearest possible percentage
			finished = False

			# Define whether rows should be added/removed to go up/down to the needed percentage
			#	True: 	(current percentage < needed percentage) -> Add more rows
			#	False: 	(current percentage > needed percentage) -> Remove from the rows
			increasing = True

			# A variable to save the point which should be the minimum number of rows
			min_point = 1
			while not finished:
				Logger.log("difference={}, finished={}, increasing={}, percentage_records={}".format(difference, finished, increasing, percentage_records))

				# Calculate the sum of row_count rows
				percent_sum = df.head(row_count)['Percent'].sum()

				# Calculate the difference between the needed percentage and the current percentage
				difference = abs(percentage - percent_sum)

				# Add it to percentage_records to prevent infinite loop
				percentage_records.append({'row_count': row_count, 'difference': difference })
				Logger.log("row_count={}, {}==?{} = {}".format(row_count, percentage, percent_sum, percent_sum == percentage))

				# current percentage is equal to the needed one
				if percent_sum == percentage:
					# while loop is finished
					finished = True
					# Add the current row count to the final list
					percentage_row_counts.append(row_count)
					Logger.log("finished={}, row_count={}".format(finished, row_count))
				elif percent_sum > percentage and increasing:
					# current percentage > needed percentage and it is still in increasing mode
					Logger.log("len(percentage_records)={}".format(len(percentage_records)))
					if len(percentage_records) > 1:
						# There are more than one result, so it is possible to find another percentage with smaller difference
						min_point = percentage_records[-2]['row_count']
						increasing = False
						Logger.log("min_point={}, increasing={}".format(min_point, increasing))
					else:
						# There is only one result in the list which means that it is impossible to go lower than this percentage
						finished = True
						percentage_row_counts.append(row_count)
						Logger.log("finished={}, row_count={}".format(finished, row_count))
						continue

				Logger.log("finished={}, increasing={}, min_point={}, percentage_records={}".format(finished, increasing, min_point, percentage_records))

				if increasing:
					# in increasing mode, double the current row_count and continue
					row_count = row_count * 2
					Logger.log("row_count={}".format(row_count))
				else:
					# in decreasing mode
					Logger.log("percent_sum= {} , percentage= {}".format(percent_sum, percentage))
					if percent_sum > percentage:
						# current percentage is higher than the needed one, so consider this as the max_point
						max_point = row_count
						Logger.log("percent_sum > percentage, max_point= {}".format(max_point))
					elif percent_sum < percentage:
						# current percentage is smaller than the needed one, so consider this as the min_point
						min_point = row_count
						Logger.log("percent_sum < percentage, min_point= {}".format(min_point))

					# find the point (row_count) in the middle between min_point and max_point
					row_count = min_point + int((max_point - min_point)/2)
					Logger.log("row_count={}".format(row_count))

					# Check if current row_count has been calculated before and stop if so
					if row_count in map(operator.itemgetter('row_count'), percentage_records):
						Logger.log("row_count in percentage_records")

						finished = True

						# Find the result with the smallest difference
						best_record = min(percentage_records, key=lambda x:x['difference'])
						row_count = best_record['row_count']
						percentage_row_counts.append(row_count)

						Logger.log("percentage: {}, percent_sum: {}, record:{}".format(percentage, percent_sum, best_record))
						continue
					Logger.log("row_count NOT in percentage_records")

			# Add the result to the CSV text
			text += "{},{} ({:.4f}%),{}/{} ({:.4f}%),{:.0f},{:.0f} ({:.2f}%), {}, {:.2f}% (\u2243 {}%)\n".format(row_count, row_count, row_count/total_row_count*100, row_count, total_row_count, row_count/total_row_count*100, total_data_values*percent_sum/100, total_data_values*percent_sum/100, row_count/total_row_count*100, total_data_values, percent_sum, percentage)
			self.repetition_matrix[percentage][self.data_chunk_series_length] ={
				"count": row_count,
				"percent": row_count/total_row_count*100
			}

		# Add the final row in the CSV text (100%)
		text += "{},{} ({:.4f}%),{}/{} ({:.4f}%),{:.0f},{:.0f} ({:.2f}%), {}, {:.2f}% (\u2243 {}%)\n".format(total_row_count, total_row_count, 100, total_row_count, total_row_count, 100, total_data_values, total_data_values, 100, total_data_values, 100, 100)

		#self.repetition_matrix[100][self.data_chunk_series_length] = total_row_count
		self.repetition_matrix[100][self.data_chunk_series_length] = {
			"count": total_row_count,
			"percent": 100
		}
		txt_file_name = self.result_file_name.format(self.data_chunk_series_length, "-summary.csv")
		fs.write(txt_file_name, text)
		Logger.plog(self.repetition_matrix)

	def save_repetition_matrix(self):
		header = "Length," + "%,".join([str(percentage) for percentage in self.repetition_matrix]) + "\n"
		percent_unique_value_count_csv = header + ""
		percent_unique_value_percent_csv = header + ""
		for length in range(1, self.data_chunk_series_length):
			try:
				percent_unique_value_percent_csv += "{},".format(length) + ",".join([str(data[length]["percent"]) for percentage, data in self.repetition_matrix.items()]) + "\n"
				percent_unique_value_count_csv += "{},".format(length) + ",".join([str(data[length]["count"]) for percentage, data in self.repetition_matrix.items()]) + "\n"
			except Exception as e:
				Logger.log("Series Length {} data doesn't exist, meaning max_rep <= 1".format(length))

		percent_unique_value_count_file = self.result_file_name.format("matrix", "-count.csv")
		fs.write(percent_unique_value_count_file, percent_unique_value_count_csv)
		self.plot_matrix(percent_unique_value_count_file)

		percent_unique_value_percent_file = self.result_file_name.format("matrix", "-percent.csv")
		fs.write(percent_unique_value_percent_file, percent_unique_value_percent_csv)
		self.plot_matrix(percent_unique_value_percent_file)

		matrix_file = self.result_file_name.format("matrix", ".json")
		fs.write_to_json(matrix_file, self.repetition_matrix)

	def fill_missing_bytes(self, d):
		for byte in range(0, 256):
			if not byte in d:
				d[byte] = 0
		return d

	def generate_plots(self, result_file_name_byte_position_based):
		generated = os.path.exists(result_file_name_byte_position_based)
		if generated:
			t_df = pd.read_csv(result_file_name_byte_position_based, sep =",", index_col="Data-Chunk", dtype={'Data-Chunk': 'Int64'}).sort_values(by=['Data-Chunk'], ascending=True)

			result_file_png = self.result_file_name.format(self.data_chunk_series_length, ".png")
			self.plot(t_df, "Data-Chunk Repetition - Based on Data-Chunk Value, {}".format(self.data_chunk_series_length), result_file_png)

			result_file_png = self.result_file_name_byte_position_based.format(self.data_chunk_series_length, ".png")
			self.plot(t_df, "Data-Chunk Repetition - Based on Data-Chunk Position, {}".format(self.data_chunk_series_length), result_file_png, reversed = True)

	def plot(self, df, subtitle, output_png, reversed = False):
		if not df.empty:
			plt.rcParams["figure.figsize"] = [14,6]
			Logger.log(df)
			ticks = [x for x in range(0, 256, 1)] #sample names
			if reversed:
				df.T.plot(kind="bar")
			else:
				plt.xlabel("Data-Chunk Value")
				df.plot(kind="bar")
				plt.xticks(ticks, fontsize=6, rotation=65)
			#plt.suptitle(subtitle)
			plt.ylabel("Data-Chunk Count")
			plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=20, mode="expand", borderaxespad=0.)
			#plt.legend(bbox_to_anchor=(0.,1.02, 1, .102), loc='upper left', borderaxespad=0., prop={'size': 8})
			plt.tight_layout()
			plt.savefig(output_png, dpi = 300)
			plt.close('all')
			plt.clf()

	def plot_matrix(self, file):
		df = pd.read_csv(file, sep =",")
		if len(df.columns) != 0:
			plt.rcParams["figure.figsize"] = [14,6]
			Logger.log(df)
			ticks = [x for x in range(0, self.data_chunk_series_length, 1)] #sample names
			#if reversed:
			#	df.T.plot(kind="bar")
			#else:
			#plt.xlabel("Data-Chunk Value")
			df.plot(x="Length")
			plt.xticks(ticks, fontsize=6, rotation=65)
			#plt.suptitle(subtitle)
			#plt.ylabel("Data-Chunk Count")
			plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=20, mode="expand", borderaxespad=0.)
			#plt.legend(bbox_to_anchor=(0.,1.02, 1, .102), loc='upper left', borderaxespad=0., prop={'size': 8})
			plt.tight_layout()
			plt.savefig(file.replace(".csv", ".png"), dpi = 300)
			plt.close('all')
			plt.clf()

	def read_binary_bits(self, path):
		data = bitarray.bitarray()
		with open(path, 'rb') as binaryfile:
			data.fromfile(binaryfile)
		return data
