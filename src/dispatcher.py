import click
import filesystem as fs
from logger import Logger
from pprint import pprint
from repetition import Repetition

class Dispatcher:
	def __init__(self, parameters):
		self.parameters = parameters
		self.logger = Logger(self.parameters['output'])

		self.parameters['cwd'] = fs.parent(fs.getcwd(__file__))
		Logger.log("+ Parameters: {}".format(self.parameters))

	def run(self):
		if not self.parameters['data_file_path'] == '':
			Logger.log("+ Processing Data File: {}".format(self.parameters['data_file_path']))

			repetition = Repetition(parameters = self.parameters)
			repetition.run()
		self.logger.close()


dispatcher = None

@click.group(chain=True, invoke_without_command=True)
@click.option('-f', '--data_file_path', required = False, default='', help='Path to a Single Data File')
@click.option('-o', '--output', required = False, default= '.', help='Output directory')
@click.option('-s', '--data_chunk_size', required = False, default= 8, help='Size of data-chunk, default = 8')
@click.option('-m', '--min_threshold', required = False, default= 1, help='Min-Threshold, default = 1')
@click.option('-x', '--max_consecutive', required = False, default= 1, help='Max-Consecutive Min-Threshold, default = 1')
@click.option('-l', '--max_series_length', required = False, default= None, help='Max-Series-Length')
@click.option('-t', '--strategy', required = True, default= 'max_rep', type=click.Choice(['max_rep', 'max_consecutive', 'max_series_length'], case_sensitive=False), help='Stopping-Point Strategy')
def main(data_file_path, output, data_chunk_size, min_threshold, max_consecutive, max_series_length, strategy):
	"""Starting DCR Processing"""
	global dispatcher
	parameters = {
		"data_file_path": data_file_path,
		'output': output,
		'data_chunk_size': data_chunk_size,
		'min_threshold': min_threshold,
		'max_consecutive': max_consecutive,
		'max_series_length': max_series_length,
		'strategy': strategy
	}
	dispatcher = Dispatcher(parameters = parameters)
	dispatcher.run()

if __name__ == '__main__':
	main()
