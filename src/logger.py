import threading, asyncio
from time import gmtime, strftime, sleep
from pprint import pprint
import filesystem as fs

def threaded(fn):
	def wrapper(*args, **kwargs):
		thread = threading.Thread(target=fn, args=args, kwargs=kwargs)
		thread.start()
		return thread
	return wrapper

class Logger:
	log_file_handler = None

	def __init__(self, path):
		log_path = "{}/logs".format(path)
		fs.mkdirs(log_path)
		log_file = self.get_name(log_path)
		Logger.log_file_handler =open(log_file, "a+")
		#Logger.log_file_handler_debug =open(log_file.replace("log_", "debug_"), "a+")
		#self.still_alive()

	def get_name(self, path):
		exists = True
		path_temp = ""
		while exists:
			time_name = strftime("%Y%m%d_%H%M%S", gmtime())
			file_name = "log_{}.log".format(time_name)
			path_temp = fs.join(path, file_name)
			exists = fs.exists(path_temp)
		return path_temp

	@staticmethod
	@threaded
	def log(text = "", end = '\n'):
		time_now = strftime("+ %Y-%m-%d %H:%M:%S: ", gmtime())
		Logger.log_file_handler.write("{}".format(time_now))
		Logger.log_file_handler.write("{}".format(text))
		Logger.log_file_handler.write("{}".format(end))
		Logger.log_file_handler.flush()

	@threaded
	def still_alive(self):
		text = 'Still Alive Notification'
		end = '\n'
		while True:
			time_now = strftime("+ %Y-%m-%d %H:%M:%S: ", gmtime())
			Logger.log_file_handler.write("{}".format(time_now))
			Logger.log_file_handler.write("{}".format(text))
			Logger.log_file_handler.write("{}".format(end))
			Logger.log_file_handler.flush()
			sleep(30)

	def plog(text):
		time_now = strftime("+ %Y-%m-%d %H:%M:%S: ", gmtime())
		text = "{} {}".format(time_now, text)
		pprint(text, Logger.log_file_handler)
		Logger.log_file_handler.flush()

	def close(self):
		Logger.log_file_handler.close()
		#Logger.log_file_handler_debug.close()

	@staticmethod
	def debug(text = "", end = '\n'):
		time_now = strftime("+ %Y-%m-%d %H:%M:%S: ", gmtime())
		#Logger.log_file_handler_debug.write("{}".format(time_now))
		#Logger.log_file_handler_debug.write("{}".format(text))
		#Logger.log_file_handler_debug.write("{}".format(end))
		#Logger.log_file_handler_debug.flush()
