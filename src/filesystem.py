import os, os.path, shutil, bitarray, csv, json

from pathlib import Path
from pprint import pprint
from itertools import zip_longest

def file_list(path, filename, size = None, recursive = True, data_type = "*"):
	files = []
	try:
		pattern = get_pattern(filename = filename, size = size, recursive = recursive, data_type = data_type)
		print("++ Path: {}, Pattern: {}".format(path, pattern))
		for file in sorted(Path(path).glob(pattern)):
			files.append(file.absolute())
	except Exception as e:
		print("++ filesystem - file - {}".format(e))
	return files

def get_pattern(filename, size = None, recursive = True, data_type = "*"):
	pattern = "**/" if recursive else None if recursive else ""
	file_size_pattern = size + "-" if size else "*"

	data_type_pattern = "{}-".format(data_type)

	if str(file_size_pattern) == str(data_type_pattern):
		pattern += data_type_pattern
	else:
		pattern += file_size_pattern + data_type_pattern

	if recursive and pattern[-1] == "*":
		pattern += "/**/"
	elif recursive and not pattern[-1] == "*":
		pattern += "*/**/"
	elif not recursive and pattern[-1] == "*":
		pattern += "/"
	else:
		pattern += "*/"

	pattern += filename
	return pattern

def touch(path):
	with open(path, 'a'):
		os.utime(path, None)

def join(*dirs):
	path = ""
	for dir in dirs:
		path = path + "/" if path != "" and  path[-1] != "/" else path
		path = os.path.join(path, dir)
	return path

def parent(path):
	return os.path.abspath(Path(path).parent.absolute())

def name(path):
	if isinstance(path, str):
		path = Path(path)
	return os.path.basename(path)

def name_no_extension(path):
	filename = name(path)
	if isinstance(filename, str):
		return filename
	return os.path.splitext(filename)[0]

def mkdirs(path):
	try:
		os.makedirs(path)
	except Exception as e:
		print("++ " + str(e))
		pass

def read_binary_bytes(path, chunk = 1):
	data = []
	with open(path, 'rb') as binaryfile:
		while True:
			bytes = binaryfile.read(chunk)
			#print('Reading file bytes: {}'.format(len(data)), end='\r')
			if not bytes:
				break
			else:
				data.append(bytes)
	print()
	return data

def read_binary_bits(path):
	data = bitarray.bitarray()
	with open(path, 'rb') as binaryfile:
		data.fromfile(binaryfile)
	return data

def read_binary_file(path):
	data = None
	with open(path, 'rb') as binaryfile:
		data = binaryfile.read()
	return data

def read_text_file(file_path):
	f = open(file_path, "r")
	data_txt  = f.read()
	return data_txt

def read_csv_file(file_path):
	text = read_text_file(file_path)
	lines = [line.split(",") for line in text.split("\n")]
	return lines

def write_dict_as_csv_file(file_path, data):
	with open('file.csv', 'w', newline='') as fout:
	    writer = csv.writer(fout)
	    writer.writerow(fieldnames)
	    writer.writerow([data['Age'], data['Name'], *data['Aliases']])

def compare_binaries(path1, path2):
    with open(path1, 'rb') as f1, open(path2, 'rb') as f2:
        for line1, line2 in zip_longest(f1, f2, fillvalue=None):
            if line1 == line2:
                continue
            else:
                return False
        return True

def file_id(file):
	file = parent(parent(file))
	id = file.split('-')[-1]
	return 	id

def file_2level_id(file):
	file = parent(parent(parent(file)))
	id = file.split('-')[-1]
	return 	id

def delete(path):
	os.remove(path)

def delete_dir(path):
	try:
		shutil.rmtree(path, ignore_errors=True)
	except Exception as e:
		print(e)

def write(path, text, append = False):
	mode = "a" if append else "w"
	f = open(path, mode)
	f.write(text)
	f.close()

def write_to_json(path, data):
	with open(path, 'w') as outfile:
		json.dump(data, outfile)
	
def exists(filepath):
	return os.path.exists(filepath)

def file_size(filename):
	try:
		statinfo = os.stat(filename)
		return statinfo.st_size
	except Exception as e:
		return 0

def file_size_byte(filename):
	return int(file_size(filename) / 8)

def copy(source, destination):
	if exists(source):
		try:
			shutil.copyfile(source, destination)
		except Exception as e:
			print(e)
			return False
		return True
	else:
		return False

def move(source, destination):
	if exists(source):
		try:
			shutil.move(source, destination)
		except Exception as e:
			print(e)
			return False
		return True
	else:
		return False

def getcwd(file):
	os.chdir(os.path.dirname(file))
	return os.getcwd()
